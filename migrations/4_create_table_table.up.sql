CREATE TABLE IF NOT EXISTS `m_table` (
	`table_id` BIGINT NOT NULL DEFAULT 0,
	`table_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`location_id` BIGINT DEFAULT 0,
	`employee_id` BIGINT NULL DEFAULT NULL,
	`tableid_parent` BIGINT NULL DEFAULT NULL,
    `useby_session_id` BIGINT NULL DEFAULT NULL,
	`useby_kassa` BIGINT NULL DEFAULT NULL,
    `useby_orderno` BIGINT NULL DEFAULT NULL,
    `useby_token` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
    `useby_timestamp` TIMESTAMP NULL DEFAULT NULL,    
    `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`table_id`) USING BTREE,
	INDEX `location_id` (`location_id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=MyISAM
;
