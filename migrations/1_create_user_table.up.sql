CREATE TABLE IF NOT EXISTS `z_user` (
	`user_id` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`user_name` VARCHAR(255) NOT NULL DEFAULT 'username' COLLATE 'utf8mb4_general_ci',
	`user_group_id` VARCHAR(20) NOT NULL DEFAULT 'USR' COLLATE 'utf8mb4_general_ci',
	`user_password` VARCHAR(500) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`user_status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
	`is_superuser` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`max_discount` DOUBLE(20,4) UNSIGNED NOT NULL DEFAULT '0',
	`can_void` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`user_id`) USING BTREE,
	INDEX `authcheck` (`user_id`, `user_password`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;