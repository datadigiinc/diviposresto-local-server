CREATE TABLE `m_menu_prichannel` (
	`channel_id` BIGINT UNSIGNED NOT NULL,
	`menu_id` BIGINT UNSIGNED NOT NULL,
	`sellingprice` DOUBLE(20,4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`channel_id`, `menu_id`)
)
COLLATE='utf8mb4_general_ci'
;
