ALTER TABLE `m_setupparameter`	
    ADD COLUMN `lastsyncdown_datetime` DATETIME NULL DEFAULT '2000-01-01 00:00:00' AFTER `tableorder_endpoint`,
	ADD COLUMN `is_syncpaused` TINYINT(4) NULL DEFAULT '1' AFTER `lastsyncdown_datetime`,
    ADD COLUMN `company_id` VARCHAR(5) NULL DEFAULT 'DD';