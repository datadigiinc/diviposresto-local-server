CREATE TABLE IF NOT EXISTS `m_coursemenu` (
	`id` BIGINT(20) UNSIGNED NOT NULL,
	`coursename` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
