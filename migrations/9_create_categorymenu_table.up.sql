CREATE TABLE IF NOT EXISTS `m_categorymenu` (
	`id` BIGINT(20) UNSIGNED NOT NULL,
	`departmentid` VARCHAR(50) NULL DEFAULT NULL,
	`categorymenu` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)

ENGINE=InnoDB;
