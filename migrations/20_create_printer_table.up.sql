CREATE TABLE `m_printer` (
	`printer_id` VARCHAR(20) NOT NULL DEFAULT '0' COLLATE 'utf8mb4_general_ci',
	`printer_descr` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`printer_active` TINYINT(3) UNSIGNED NULL DEFAULT '0',
	`printer_name` VARCHAR(250) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci', 
	`feedcut` TINYINT(3) NULL DEFAULT '4',
	PRIMARY KEY (`printer_id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
