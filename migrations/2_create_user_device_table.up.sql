CREATE TABLE IF NOT EXISTS `z_devices` (
	`device_id` VARCHAR(100) DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`device_name` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`appversion` BIGINT NOT NULL DEFAULT 0,
	`last_login_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`last_user_id` VARCHAR(20) DEFAULT '' COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`device_id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;