CREATE TABLE IF NOT EXISTS `y_postmp_detail` (
	`session_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`user_id` VARCHAR(20) NOT NULL DEFAULT '1' COLLATE 'utf8_general_ci',
	`orderno` BIGINT(20) NOT NULL DEFAULT 0 COLLATE 'utf8_general_ci',
	`branch_id` VARCHAR(20) NOT NULL COLLATE 'utf8_general_ci',
	`menu_id` BIGINT(20) NOT NULL DEFAULT 0 COLLATE 'utf8_general_ci',
	`ref_menuid` BIGINT(20) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`seqno` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`detailseqno` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`menuname` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`qty` DOUBLE(20,4) NOT NULL DEFAULT '0.0000',
	`uom` VARCHAR(10) NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`hjual` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`oldhjual` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`hpp` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`discpercent` DOUBLE(10,4) NULL DEFAULT '0.0000',
	`discamount` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`discamount2` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`ppn_detail` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`totalamount` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`discamount_promo` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`promono` VARCHAR(20) NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`promotype` VARCHAR(10) NULL DEFAULT '' COLLATE 'utf8_general_ci',
	`stasendto` TINYINT(3) NOT NULL DEFAULT '0',
	`stasenddone` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`stasendcapt` TINYINT(3) NULL DEFAULT '0',
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`working_at` TIMESTAMP NULL DEFAULT NULL,
	`done_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`session_id`, `orderno`, `branch_id`, `seqno`, `detailseqno`, `menu_id`) USING BTREE,
	INDEX `ordernodetail` (`branch_id`, `user_id`, `orderno`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
