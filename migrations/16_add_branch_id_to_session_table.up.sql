ALTER TABLE `y_session`
	ADD COLUMN `branch_id` VARCHAR(20) NOT NULL DEFAULT 'DDR' AFTER `session_id`,
	DROP INDEX `key1`,
	ADD UNIQUE INDEX `key1` (`session_id`, `branch_id`, `user_id`);