CREATE TABLE IF NOT EXISTS `y_pos_detail` (
	`session_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`branch_id` VARCHAR(20) NOT NULL COLLATE 'utf8mb4_general_ci',
	`strukdate` DATE NOT NULL DEFAULT '0000-00-00',
	`strukno` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`orderno` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`menu_id` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`ref_menuid` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`seqno` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`detailseqno` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`menuname` VARCHAR(50) NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`qty` DOUBLE(20,4) NOT NULL DEFAULT '0.0000',
	`uom` VARCHAR(10) NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`hjual` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`oldhjual` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`hpp` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`discpercent` DOUBLE(10,4) NULL DEFAULT '0.0000',
	`discamount` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`discamount2` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`ppn_detail` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`totalamount` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`discamount_promo` DOUBLE(20,4) NULL DEFAULT '0.0000',
	`promono` VARCHAR(20) NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`promotype` VARCHAR(10) NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`branch_id`, `strukdate`, `strukno`, `seqno`, `detailseqno`) USING BTREE,
	INDEX `menu` (`menuid`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
