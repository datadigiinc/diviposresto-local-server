CREATE TABLE `m_location` (
	`location_id` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`location_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`seqno` INT(10) NULL DEFAULT NULL,
	PRIMARY KEY (`location_id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
