CREATE TABLE IF NOT EXISTS `y_session` (
	`session_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`user_id` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8mb4_general_ci',
	`device_id` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`tipe_anjungan` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`opendate` DATE NULL DEFAULT NULL,
	`opentime` TIME NULL DEFAULT NULL,
	`closedate` DATE NULL DEFAULT NULL,
	`closetime` TIME NULL DEFAULT NULL,
	`openbalance` DOUBLE(20,4) UNSIGNED NOT NULL DEFAULT '0.0000',
	`closebalance` DOUBLE(20,4) UNSIGNED NOT NULL DEFAULT '0.0000',
	`trxcount` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`strukno_start` VARCHAR(10) NULL DEFAULT '0000000000' COLLATE 'utf8mb4_general_ci',
	`strukno_end` VARCHAR(10) NULL DEFAULT '0000000000' COLLATE 'utf8mb4_general_ci',
	`void_count` INT(10) NULL DEFAULT '0',
	PRIMARY KEY (`session_id`) USING BTREE,
	UNIQUE INDEX `key1` (`user_id`, `opendate`, `opentime`, `closedate`, `closetime`) USING BTREE	
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
