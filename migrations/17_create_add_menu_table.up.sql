CREATE TABLE `m_menuadd` (
	`menuid` VARCHAR(20) NOT NULL DEFAULT '',
	`menuid_add` VARCHAR(20) NOT NULL DEFAULT '',
	`sellingprice` DOUBLE(20,4) UNSIGNED NOT NULL DEFAULT '0.0000',
	PRIMARY KEY (`menuid`, `menuid_add`) USING BTREE
)

ENGINE=InnoDB
;
